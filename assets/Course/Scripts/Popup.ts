// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Popup extends cc.Component {
    @property(cc.Label)
    txtCombo: cc.Label = null;
    start() {
        const randomNumber = Math.random();
        const randomNumberBetween1And1 = randomNumber * 2 - 1;
        let dir = 0;
        if (randomNumberBetween1And1 < 0) {
            dir = -1;
        } else if (randomNumberBetween1And1 > 0) {
            dir = 1;
        } else {
            dir = 0;
        }
                
        cc.tween(this.node)
            .to(0.5, { position: new cc.Vec2(this.node.position.x + (100 * dir), this.node.position.y + 100) }).call(() => {
                this.node.destroy();
            })
            .start()
    }
}
