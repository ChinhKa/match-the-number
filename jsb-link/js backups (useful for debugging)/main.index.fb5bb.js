window.__require = function t(e, o, n) {
function i(s, r) {
if (!o[s]) {
if (!e[s]) {
var a = s.split("/");
a = a[a.length - 1];
if (!e[a]) {
var p = "function" == typeof __require && __require;
if (!r && p) return p(a, !0);
if (c) return c(a, !0);
throw new Error("Cannot find module '" + s + "'");
}
s = a;
}
var d = o[s] = {
exports: {}
};
e[s][0].call(d.exports, function(t) {
return i(e[s][1][t] || t);
}, d, d.exports, t, e, o, n);
}
return o[s].exports;
}
for (var c = "function" == typeof __require && __require, s = 0; s < n.length; s++) i(n[s]);
return i;
}({
Animation_Control: [ function(t, e) {
"use strict";
cc._RF.push(e, "3b07df7g99M3IMFXQOCIsVh", "Animation_Control");
cc.Class({
extends: cc.Component,
properties: {
Animation_Node: cc.Animation
},
Play_Walk: function() {
this.Animation_Node.play("Walk");
},
Play_Cheer: function() {
this.Animation_Node.playAdditive("Cheer");
}
});
cc._RF.pop();
}, {} ],
Button_Test: [ function(t, e) {
"use strict";
cc._RF.push(e, "e4425sOHZ9NrrhPyrDCEXQS", "Button_Test");
cc.Class({
extends: cc.Component,
properties: {},
Clicked: function() {
cc.log("CLICKED!");
}
});
cc._RF.pop();
}, {} ],
DataManager: [ function(t, e, o) {
"use strict";
cc._RF.push(e, "2c16btt3lVEZYtZ3DhuUQrr", "DataManager");
Object.defineProperty(o, "__esModule", {
value: !0
});
var n = cc._decorator, i = n.ccclass, c = (n.property, function(t) {
__extends(e, t);
function e() {
var e = null !== t && t.apply(this, arguments) || this;
e.listReward = [ {
size: 512,
reward: 100
}, {
size: 1024,
reward: 200
}, {
size: 2048,
reward: 300
}, {
size: 4096,
reward: 400
} ];
return e;
}
o = e;
e.prototype.onLoad = function() {
o.ins = this;
};
var o;
e.ins = null;
return o = __decorate([ i ], e);
}(cc.Component));
o.default = c;
cc._RF.pop();
}, {} ],
EventSystem: [ function(t, e, o) {
"use strict";
cc._RF.push(e, "309f50KhrdOVp8fOA9CI77m", "EventSystem");
Object.defineProperty(o, "__esModule", {
value: !0
});
var n = cc._decorator, i = n.ccclass, c = n.property, s = function(t) {
__extends(e, t);
function e() {
var e = null !== t && t.apply(this, arguments) || this;
e.label = null;
e.text = "hello";
return e;
}
e.prototype.onLoad = function() {
cc.director.getPhysicsManager().enabled = !0;
cc.director.getCollisionManager().enabled = !0;
};
e.prototype.start = function() {};
__decorate([ c(cc.Label) ], e.prototype, "label", void 0);
__decorate([ c ], e.prototype, "text", void 0);
return __decorate([ i ], e);
}(cc.Component);
o.default = s;
cc._RF.pop();
}, {} ],
GameConstants: [ function(t, e, o) {
"use strict";
cc._RF.push(e, "1ac4fSdjUJGiqWWpOX+lLkX", "GameConstants");
Object.defineProperty(o, "__esModule", {
value: !0
});
var n, i = cc._decorator, c = i.ccclass, s = (i.property, function(t) {
__extends(e, t);
function e() {
return null !== t && t.apply(this, arguments) || this;
}
return __decorate([ c ], e);
}(cc.Component));
o.default = s;
(function(t) {
t[t.grid = 1] = "grid";
})(n || (n = {}));
cc._RF.pop();
}, {} ],
GameManager: [ function(t, e, o) {
"use strict";
cc._RF.push(e, "8dcf8zDaS9OTK3vhbB1TKRH", "GameManager");
Object.defineProperty(o, "__esModule", {
value: !0
});
var n = t("./DataManager"), i = t("./Grid"), c = t("./NodeNumber"), s = t("./UIManager"), r = cc._decorator, a = r.ccclass, p = r.property, d = function(t) {
__extends(e, t);
function e() {
var e = null !== t && t.apply(this, arguments) || this;
e.label = null;
e.redColor = cc.Color.RED;
e.blueColor = cc.Color.BLUE;
e.greenColor = cc.Color.GREEN;
e.yellowColor = cc.Color.YELLOW;
e.orangeColor = cc.Color.ORANGE;
e.cyanColor = cc.Color.CYAN;
e.grayColor = cc.Color.GRAY;
e.listNumber = [ 2, 4, 8, 16, 32, 64, 128 ];
e.pre_Node = null;
e.pre_Grid = null;
e.queue = [];
e.listNode = [];
e.endGame = !1;
e.useBreakItem = !1;
e.useSwapItem = !1;
e.listNode_Swap = [];
e.numbersCombo = 0;
e.countDownCombo = 0;
e.enableTween_Combo = !0;
return e;
}
o = e;
e.prototype.onLoad = function() {
o.ins = this;
};
e.prototype.start = function() {
this.SpawGrid(5, 7);
this.SpawNode(5, 2);
this.CheckGridPlaced();
};
e.prototype.update = function() {};
e.prototype.SpawGrid = function(t, e) {
for (var o = 0; o < t; o++) for (var n = 0; n < e; n++) {
var c = cc.instantiate(this.pre_Grid);
this.node.addChild(c);
c.setPosition(cc.v2(170 * o - 340, 170 * n - 300));
c.getComponent(i.default).isPlaced = !1;
c.getComponent(i.default).position = c.position;
this.queue.push(c.getComponent(i.default));
console.log(o + "+" + c.position);
}
};
e.prototype.SpawNode = function(t, e) {
for (var o = 0, n = 0; n < t; n++) for (var i = 0; i < e; i++) {
var s = cc.instantiate(this.pre_Node), r = Math.floor(Math.random() * (this.listNumber.length - 0)) + 0;
s.getComponent(c.default).setNumber(this.listNumber[r]);
s.getComponent(c.default).ID = o;
this.listNode.push(s);
this.SelectNodeColor(s);
this.node.addChild(s);
s.setPosition(cc.v2(170 * n - 340, 170 * i - 300));
o++;
}
};
e.prototype.CheckGridPlaced = function() {
var t = this;
try {
for (var e = 0; e < this.queue.length; e++) this.queue[e].isPlaced = !1;
for (var o = function(e) {
var o = n.queue.find(function(o) {
return o.position.x === t.listNode[e].position.x && o.position.y === t.listNode[e].position.y;
});
null != o && (o.getComponent(i.default).isPlaced = !0);
}, n = this, c = 0; c < this.listNode.length; c++) o(c);
} catch (t) {
console.log("Lỗi" + t);
}
};
e.prototype.SelectNodeColor = function(t) {
switch (t.getComponent(c.default).number) {
case 2:
t.color = this.redColor;
break;

case 4:
t.color = this.blueColor;
break;

case 8:
t.color = this.greenColor;
break;

case 16:
t.color = this.yellowColor;
break;

case 32:
t.color = this.orangeColor;
break;

case 64:
t.color = this.cyanColor;
break;

case 128:
t.color = this.grayColor;
}
};
e.prototype.DropDownNode = function(t) {
for (var e = 0; e < this.listNode.length; e++) this.listNode[e].getComponent(c.default).ID != t && this.listNode[e].getComponent(c.default).DropDown();
};
e.prototype.PushNode = function() {
var t = this;
setTimeout(function() {
if (!t.endGame) for (var e = 6; e < 41; e += 7) {
t.CheckGridPlaced();
if (!t.queue[e].isPlaced) {
for (var o = 0; o < t.listNode.length; o++) t.queue[e].position.x === t.listNode[o].position.x && t.listNode[o].setPosition(new cc.Vec3(t.listNode[o].position.x, t.listNode[o].position.y + 170, 0));
var n = cc.instantiate(t.pre_Node), i = Math.floor(Math.random() * (t.listNumber.length - 0)) + 0;
n.getComponent(c.default).setNumber(t.listNumber[i]);
t.listNode.push(n);
t.SelectNodeColor(n);
t.node.addChild(n);
n.setPosition(t.queue[e - 6].position);
}
}
}, 200);
};
e.prototype.UseBreakItem = function() {
this.useBreakItem = !0;
};
e.prototype.UseSwapItem = function() {
this.useSwapItem = !0;
};
e.prototype.SwapItems = function() {
if (this.listNode_Swap.length >= 2 && this.useSwapItem) {
this.useSwapItem = !1;
this.node_Temp = this.listNode_Swap[0].position;
this.listNode_Swap[0].position = this.listNode_Swap[1].position;
this.listNode_Swap[0].getComponent(c.default).old_Pos = this.listNode_Swap[1].position;
this.listNode_Swap[1].position = this.node_Temp;
this.listNode_Swap[1].getComponent(c.default).old_Pos = this.node_Temp;
cc.tween(this.listNode_Swap[0]).to(0, {
scale: 1
}).start();
cc.tween(this.listNode_Swap[1]).to(0, {
scale: 1
}).start();
for (;this.listNode_Swap.length > 0; ) this.listNode_Swap.pop();
}
};
e.prototype.CheckReward = function(t) {
for (var e = 0; e < n.default.ins.listReward.length; e++) if (parseInt(n.default.ins.listReward[e].size) === parseInt(t)) {
var o = parseInt(cc.sys.localStorage.getItem("coin")) + parseInt(n.default.ins.listReward[e].reward);
cc.sys.localStorage.setItem("coin", o);
}
};
e.prototype.CheckNumbersCombo = function() {
var t = this;
if (this.numbersCombo >= 2) {
s.default.ins.txtCombo.string = "Combo X" + this.numbersCombo;
if (this.enableTween_Combo) {
this.enableTween_Combo = !1;
cc.tween(s.default.ins.txtCombo.node).to(.2, {
scale: 2
}).call(function() {
cc.tween(s.default.ins.txtCombo.node).to(0, {
scale: 0
}).call(function() {
t.enableTween_Combo = !0;
}).start();
}).start();
}
} else s.default.ins.txtCombo.string = "";
};
e.prototype.ReloadScene = function() {
cc.sys.localStorage.setItem("currentPoint", 0);
cc.director.loadScene("SceneMain");
};
e.prototype.CheckLose = function() {
for (var t = 0; t < this.listNode.length; t++) ;
};
var o;
e.ins = null;
__decorate([ p(cc.Label) ], e.prototype, "label", void 0);
__decorate([ p ], e.prototype, "listNumber", void 0);
__decorate([ p(cc.Prefab) ], e.prototype, "pre_Node", void 0);
__decorate([ p(cc.Prefab) ], e.prototype, "pre_Grid", void 0);
return o = __decorate([ a ], e);
}(cc.Component);
o.default = d;
cc._RF.pop();
}, {
"./DataManager": "DataManager",
"./Grid": "Grid",
"./NodeNumber": "NodeNumber",
"./UIManager": "UIManager"
} ],
Grid: [ function(t, e, o) {
"use strict";
cc._RF.push(e, "ad1d9U8oI9BFrTLZAMbo/d4", "Grid");
Object.defineProperty(o, "__esModule", {
value: !0
});
var n = cc._decorator, i = n.ccclass, c = (n.property, function(t) {
__extends(e, t);
function e() {
var e = null !== t && t.apply(this, arguments) || this;
e.isPlaced = null;
e.position = null;
return e;
}
return __decorate([ i ], e);
}(cc.Component));
o.default = c;
cc._RF.pop();
}, {} ],
NodeNumber: [ function(t, e, o) {
"use strict";
cc._RF.push(e, "c26b6thQxxHD5Vk2gtgF0rz", "NodeNumber");
Object.defineProperty(o, "__esModule", {
value: !0
});
var n = t("./GameManager"), i = t("./Grid"), c = t("./UIManager"), s = cc._decorator, r = s.ccclass, a = s.property, p = function(t) {
__extends(e, t);
function e() {
var e = null !== t && t.apply(this, arguments) || this;
e.txtNumber = null;
e.number = null;
e.ID = null;
e.isDragging = !1;
e.isDropped = !1;
e.canStillPull = !1;
e.touchOffset = cc.Vec3.ZERO;
e.old_PosX = null;
e.enabledDrop = !0;
e.minX = -425;
e.maxX = 425;
e.minY = -300;
e.maxY = 720;
return e;
}
o = e;
e.prototype.onLoad = function() {
this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
};
e.prototype.start = function() {
this.old_Pos = this.node.position;
};
e.prototype.onTouchStart = function(t) {
this.old_PosX = this.node.position.x;
if (n.default.ins.useBreakItem) {
n.default.ins.useBreakItem = !1;
this.DestroyNode();
n.default.ins.DropDownNode(this.ID);
} else if (n.default.ins.useSwapItem) {
if (n.default.ins.listNode_Swap.length <= 2) {
cc.tween(this.node).to(0, {
scale: 1.1
}).start();
n.default.ins.listNode_Swap.push(this.node);
n.default.ins.SwapItems();
}
return;
}
for (var e = 0; e < n.default.ins.queue.length; e++) if (n.default.ins.queue[e].isPlaced && Math.round(n.default.ins.queue[e].position.x) === Math.round(this.node.position.x) && Math.round(n.default.ins.queue[e].position.y) === Math.round(this.node.position.y)) {
n.default.ins.queue[e].isPlaced = !1;
break;
}
this.isDragging = !0;
this.isDropped = !1;
var o = t.getLocation();
this.touchOffset = this.node.position.sub(this.node.parent.convertToNodeSpaceAR(o));
};
e.prototype.onTouchMove = function(t) {
if (this.isDragging) {
var e = t.getLocation(), o = this.node.parent.convertToNodeSpaceAR(e).add(this.touchOffset);
o.x < this.minX && (o.x = this.minX);
o.x > this.maxX && (o.x = this.maxX);
o.y < this.minY && (o.y = this.minY);
o.y > this.maxY && (o.y = this.maxY);
this.node.position = o;
if (this.enabledDrop) {
n.default.ins.DropDownNode(this.ID);
this.enabledDrop = !1;
}
}
};
e.prototype.onTouchEnd = function() {
n.default.ins.useSwapItem || cc.tween(this.node).to(0, {
scale: 1
}).start();
this.isDragging = !1;
this.isDropped = !0;
this.enabledDrop = !0;
};
e.prototype.onTouchCancel = function() {
this.isDropped = !0;
this.isDragging = !1;
this.enabledDrop = !0;
};
e.prototype.onDestroy = function() {
this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
};
e.prototype.setNumber = function(t) {
this.number = t;
this.txtNumber.string = t + "";
};
e.prototype.onCollisionStay = function(t) {
if (this.isDropped) {
this.isDropped = !1;
if (1 === t.tag) {
t.node.getComponent(i.default).isPlaced ? t.node.getComponent(i.default).number === this.number ? this.node.setPosition(t.node.getPosition()) : this.node.position = this.old_Pos : this.node.setPosition(t.node.getPosition());
n.default.ins.DropDownNode(-1);
Math.round(this.node.position.x) != this.old_PosX && n.default.ins.PushNode();
}
}
if (0 === t.tag && !this.isDragging && !t.node.getComponent(o).isDragging && this.number === t.node.getComponent(o).number) {
var e = 2 * this.number;
t.node.getComponent(o).setNumber(e);
t.node.getComponent(o).Hover();
var s = Number(cc.sys.localStorage.getItem("currentPoint")) + e;
cc.sys.localStorage.setItem("currentPoint", s);
Number(cc.sys.localStorage.getItem("maxPoint")) <= s && cc.sys.localStorage.setItem("maxPoint", s);
this.DestroyNode();
n.default.ins.CheckReward(e);
c.default.ins.ShowText();
n.default.ins.SelectNodeColor(t.node);
n.default.ins.numbersCombo++;
n.default.ins.countDownCombo = 3;
n.default.ins.DropDownNode(-1);
}
};
e.prototype.Hover = function() {
var t = this;
cc.tween(this.node).to(.1, {
scale: 1.2
}).call(function() {
cc.tween(t.node).to(.1, {
scale: 1
}).start();
}).start();
};
e.prototype.DropDown = function() {
if (!this.isDragging) for (var t = n.default.ins.queue.length - 1; t >= 0; t--) {
n.default.ins.CheckGridPlaced();
if (!n.default.ins.queue[t].isPlaced && this.node.position.y > -300 && Math.round(n.default.ins.queue[t].position.x) === Math.round(this.node.position.x)) {
this.node.setPosition(new cc.Vec2(this.node.position.x, n.default.ins.queue[t].position.y));
this.old_Pos = n.default.ins.queue[t].position;
}
}
};
e.prototype.DestroyNode = function() {
var t = this, e = n.default.ins.listNode.findIndex(function(e) {
return e.position.x === t.node.position.x && e.position.y === t.node.position.y;
});
n.default.ins.listNode.splice(e, 1);
this.node.destroy();
};
var o;
__decorate([ a(cc.Label) ], e.prototype, "txtNumber", void 0);
__decorate([ a ], e.prototype, "number", void 0);
return o = __decorate([ r ], e);
}(cc.Component);
o.default = p;
cc._RF.pop();
}, {
"./GameManager": "GameManager",
"./Grid": "Grid",
"./UIManager": "UIManager"
} ],
SoundManager: [ function(t, e, o) {
"use strict";
cc._RF.push(e, "84e36QgbCNKBo7TpIGjOTFY", "SoundManager");
Object.defineProperty(o, "__esModule", {
value: !0
});
var n = cc._decorator, i = n.ccclass, c = (n.property, function(t) {
__extends(e, t);
function e() {
return null !== t && t.apply(this, arguments) || this;
}
o = e;
e.prototype.onLoad = function() {
o.ins = this;
};
e.prototype.start = function() {};
e.prototype.Mute = function() {};
e.prototype.UnMute = function() {};
e.prototype.Vibrate = function() {};
e.prototype.UnVibrate = function() {};
var o;
e.ins = null;
return o = __decorate([ i ], e);
}(cc.Component));
o.default = c;
cc._RF.pop();
}, {} ],
Swipe_Camera: [ function(t, e) {
"use strict";
cc._RF.push(e, "7ff00JIeQlPRYfipylTeKVM", "Swipe_Camera");
cc.Class({
extends: cc.Component,
properties: {
Camera_Base: cc.Node
},
onLoad: function() {
this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this, !0);
this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this, !0);
this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnded, this, !0);
this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancelled, this, !0);
this.touch_moved = !1;
this.pos_offset = cc.v2();
},
onTouchBegan: function(t) {
this.touch_moved = !1;
this.stopPropagationIfTargetIsMe(t);
},
onTouchEnded: function(t) {
this.touch_moved ? t.stopPropagation() : this.stopPropagationIfTargetIsMe(t);
},
onTouchCancelled: function(t) {
this.stopPropagationIfTargetIsMe(t);
},
onTouchMove: function(t) {
var e = t.getTouches()[0].getDelta();
this.pos_offset.x = e.x;
this.pos_offset.y = e.y;
var o = this.Camera_Base.getPosition();
o.x = o.x - this.pos_offset.x;
o.z = o.z + this.pos_offset.y;
this.Camera_Base.setPosition(o);
this.Cancel_Inner_Touch(t);
this.stopPropagationIfTargetIsMe(t);
},
stopPropagationIfTargetIsMe: function(t) {
t.eventPhase === cc.Event.AT_TARGET && t.target === this.node && t.stopPropagation();
},
Cancel_Inner_Touch: function(t) {
var e = t.touch;
if (e.getLocation().sub(e.getStartLocation()).mag() > 7 && !this.touch_moved && t.target !== this.node) {
var o = new cc.Event.EventTouch(t.getTouches(), t.bubbles);
o.type = cc.Node.EventType.TOUCH_CANCEL;
o.touch = t.touch;
o.simulate = !0;
t.target.dispatchEvent(o);
this.touch_moved = !0;
}
}
});
cc._RF.pop();
}, {} ],
Test: [ function(t, e, o) {
"use strict";
cc._RF.push(e, "96cb4SjgjtCXIWFcZam5E23", "Test");
Object.defineProperty(o, "__esModule", {
value: !0
});
var n = cc._decorator, i = n.ccclass, c = n.property, s = function(t) {
__extends(e, t);
function e() {
var e = null !== t && t.apply(this, arguments) || this;
e.label = null;
e.text = "hello";
e.speed = 200;
return e;
}
e.prototype.onLoad = function() {
cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
cc.director.getCollisionManager().enabled = !0;
cc.director.getPhysicsManager().enabled = !0;
};
e.prototype.start = function() {};
e.prototype.onKeyDown = function(t) {
switch (t.keyCode) {
case cc.macro.KEY.left:
this.node.x -= this.speed * cc.director.getDeltaTime();
break;

case cc.macro.KEY.right:
this.node.x += this.speed * cc.director.getDeltaTime();
break;

case cc.macro.KEY.up:
this.node.y += this.speed * cc.director.getDeltaTime();
break;

case cc.macro.KEY.down:
this.node.y -= this.speed * cc.director.getDeltaTime();
}
};
e.prototype.update = function() {};
e.prototype.onKeyUp = function() {};
e.prototype.onDestroy = function() {
cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
};
e.prototype.onCollisionEnter = function() {
console.log(123);
};
__decorate([ c(cc.Label) ], e.prototype, "label", void 0);
__decorate([ c ], e.prototype, "text", void 0);
__decorate([ c ], e.prototype, "speed", void 0);
return __decorate([ i ], e);
}(cc.Component);
o.default = s;
cc._RF.pop();
}, {} ],
UIManager: [ function(t, e, o) {
"use strict";
cc._RF.push(e, "c031ddfDmFBU4d78CnUY/6i", "UIManager");
Object.defineProperty(o, "__esModule", {
value: !0
});
var n = cc._decorator, i = n.ccclass, c = n.property, s = function(t) {
__extends(e, t);
function e() {
var e = null !== t && t.apply(this, arguments) || this;
e.endGameUI = null;
e.winGameUI = null;
e.pauseGameUI = null;
e.txtCoin = null;
e.txtMaxPoint = null;
e.txtCurrentPoint = null;
e.txtCombo = null;
return e;
}
o = e;
e.prototype.onLoad = function() {
o.ins = this;
this.ActiveUI(null);
};
e.prototype.start = function() {
null === cc.sys.localStorage.getItem("coin") && cc.sys.localStorage.setItem("coin", 500);
null === cc.sys.localStorage.getItem("maxPoint") && cc.sys.localStorage.setItem("maxPoint", 0);
null === cc.sys.localStorage.getItem("currentPoint") && cc.sys.localStorage.setItem("currentPoint", 0);
null === cc.sys.localStorage.getItem("endTurn") && cc.sys.localStorage.setItem("endTurn", 0);
null === cc.sys.localStorage.getItem("goal") && cc.sys.localStorage.setItem("goal", -1);
this.ShowText();
};
e.prototype.ShowText = function() {
this.txtCoin.string = cc.sys.localStorage.getItem("coin");
this.txtMaxPoint.string = cc.sys.localStorage.getItem("maxPoint");
this.txtCurrentPoint.string = cc.sys.localStorage.getItem("currentPoint");
};
e.prototype.ActiveUI = function(t) {
this.endGameUI.active = !1;
this.winGameUI.active = !1;
this.pauseGameUI.active = !1;
null != t && (t.active = !0);
};
e.prototype.ShowPauseUI = function() {
this.ActiveUI(this.pauseGameUI);
};
e.prototype.HiddenPauseUI = function() {
this.ActiveUI(null);
};
var o;
e.ins = null;
__decorate([ c(cc.Node) ], e.prototype, "endGameUI", void 0);
__decorate([ c(cc.Node) ], e.prototype, "winGameUI", void 0);
__decorate([ c(cc.Node) ], e.prototype, "pauseGameUI", void 0);
__decorate([ c(cc.Label) ], e.prototype, "txtCoin", void 0);
__decorate([ c(cc.Label) ], e.prototype, "txtMaxPoint", void 0);
__decorate([ c(cc.Label) ], e.prototype, "txtCurrentPoint", void 0);
__decorate([ c(cc.Label) ], e.prototype, "txtCombo", void 0);
return o = __decorate([ i ], e);
}(cc.Component);
o.default = s;
cc._RF.pop();
}, {} ],
bomb_control_2: [ function(t, e) {
"use strict";
cc._RF.push(e, "888c6K68MFDBoFSTBHusoJM", "bomb_control_2");
cc.Class({
extends: cc.Component,
properties: {
Explosion_Node: cc.Node
},
onLoad: function() {},
Explode: function() {
this.scheduleOnce(this.Explode_Function, .15);
},
Explode_Function: function() {
this.Explosion_Node.parent = this.node.parent;
this.Explosion_Node.setPosition(this.node.getPosition());
this.Explosion_Node.active = !0;
this.node.destroy();
},
onPostSolve: function(t) {
t.getImpulse().normalImpulses > 2e3 && this.Explode_Function();
}
});
cc._RF.pop();
}, {} ],
bomb_control: [ function(t, e) {
"use strict";
cc._RF.push(e, "5543dxmYKZL1ZwGGYahLdGj", "bomb_control");
cc.Class({
extends: cc.Component,
properties: {},
Remove_Node: function() {
this.node.destroy();
},
onBeginContact: function(t, e, o) {
o.node.player_control && this.node.getComponent(cc.Animation).play();
}
});
cc._RF.pop();
}, {} ],
camera_control: [ function(t, e) {
"use strict";
cc._RF.push(e, "4cf85Gf+YdEuaByETvNn78e", "camera_control");
cc.Class({
extends: cc.Component,
properties: {
Player_Node: cc.Node,
BG_Layer_Back: cc.Node,
BG_Layer_Mid: cc.Node
},
update: function() {
var t = this.Player_Node.getPosition();
t.y = cc.misc.clampf(t.y, 0, 220);
var e = this.node.getPosition();
e.lerp(t, .1, e);
this.node.setPosition(e);
this.BG_Layer_Back.setPosition(e.x / 2, e.y / 2);
this.BG_Layer_Mid.setPosition(e.x / 4, e.y / 4);
}
});
cc._RF.pop();
}, {} ],
debug_body: [ function(t, e) {
"use strict";
cc._RF.push(e, "3a5e90jKVFJ95F0loniKZ3L", "debug_body");
cc.Class({
extends: cc.Component,
properties: {},
onLoad: function() {
window.Test_Body = this.node.getComponent(cc.RigidBody);
}
});
cc._RF.pop();
}, {} ],
event_demo: [ function(t, e) {
"use strict";
cc._RF.push(e, "dd950w+YepFvYz7su27rtck", "event_demo");
cc.Class({
extends: cc.Component,
properties: {},
Emit_Red: function() {
this.node.emit("RED");
},
Emit_Green: function() {
this.node.emit("GREEN");
}
});
cc._RF.pop();
}, {} ],
explosion_effect: [ function(t, e) {
"use strict";
cc._RF.push(e, "8c14aOzmEBCSYfLplZgIa7A", "explosion_effect");
cc.Class({
extends: cc.Component,
properties: {},
onBeginContact: function(t, e, o) {
var n = o.node.getComponent("bomb_control_2");
n && n.Explode();
}
});
cc._RF.pop();
}, {} ],
explosion_force: [ function(t, e) {
"use strict";
cc._RF.push(e, "44eb5tXmlxEM75uXuLZTCVL", "explosion_force");
cc.Class({
extends: cc.Component,
properties: {
Bomb_Root: cc.Node
},
onBeginContact: function(t, e, o) {
if (o.body.type === cc.RigidBodyType.Dynamic && o.node !== this.Bomb_Root) {
var n = o.node.getPosition(), i = this.Bomb_Root.getPosition(), c = n.sub(i);
c.normalizeSelf();
c.mulSelf(2e5);
o.body.applyForceToCenter(c);
}
}
});
cc._RF.pop();
}, {} ],
glass_control: [ function(t, e) {
"use strict";
cc._RF.push(e, "60a6fFIeOFANbD2RiHyWv99", "glass_control");
cc.Class({
extends: cc.Component,
editor: {
executeInEditMode: !0
},
properties: {
My_Sprite: cc.Sprite,
Effect_Ratio: 1.4
},
onLoad: function() {
if (this.My_Sprite) {
this.FX_Material = this.My_Sprite.getMaterial(0);
var t = this.My_Sprite.node.height / this.My_Sprite.node.width;
this.FX_Material.setProperty("Width_Scale", t, 0);
}
},
update: function() {
if (this.My_Sprite) {
var t = this.node.convertToWorldSpaceAR(cc.Vec2.ZERO);
(t = this.My_Sprite.node.convertToNodeSpaceAR(t)).x = t.x / this.My_Sprite.node.width;
t.y = t.y / this.My_Sprite.node.height;
this.FX_Material.setProperty("Effect_Position", [ t.x, t.y ], 0);
this.FX_Material.setProperty("Effect_Ratio", this.Effect_Ratio, 0);
}
}
});
cc._RF.pop();
}, {} ],
main_game_control: [ function(t, e) {
"use strict";
cc._RF.push(e, "fe2447VH5RJkqVKWZCV9bot", "main_game_control");
var o;
(o = t("./camera_control")) && o.__esModule;
cc.Class({
extends: cc.Component,
properties: {},
onLoad: function() {
var t = cc.director.getPhysicsManager();
t.enabled = !0;
t.gravity = cc.v2(0, -2e3);
cc.director.getPhysicsManager().debugDrawFlags = cc.PhysicsManager.DrawBits.e_aabbBit | cc.PhysicsManager.DrawBits.e_pairBit | cc.PhysicsManager.DrawBits.e_centerOfMassBit | cc.PhysicsManager.DrawBits.e_jointBit | cc.PhysicsManager.DrawBits.e_shapeBit;
}
});
cc._RF.pop();
}, {
"./camera_control": "camera_control"
} ],
my_progress_bar: [ function(t, e) {
"use strict";
cc._RF.push(e, "f3651ie1QRKBpyyJ+jOBB+W", "my_progress_bar");
cc.Class({
extends: cc.Component,
properties: {
Bar_Sprite: cc.Sprite
},
onLoad: function() {
this.progress = 0;
this.Bar_Sprite.fillRange = this.progress;
},
Fill_The_Bar: function() {
this.progress += .1;
this.Bar_Sprite.fillRange = this.progress;
}
});
cc._RF.pop();
}, {} ],
observer_01: [ function(t, e) {
"use strict";
cc._RF.push(e, "0a8c5IcHflPapr9HhOtJfqM", "observer_01");
cc.Class({
extends: cc.Component,
properties: {
Subject_Node: cc.Node
},
onLoad: function() {
this.Subject_Node.on("GREEN", this.Become_Green, this);
this.Subject_Node.on("RED", this.Become_Red, this);
},
Become_Red: function() {
this.node.color = cc.color(255, 0, 0, 255);
},
Become_Green: function() {
this.node.color = cc.color(0, 255, 0, 255);
}
});
cc._RF.pop();
}, {} ],
observer_02: [ function(t, e) {
"use strict";
cc._RF.push(e, "c2ee5vymaFBy7qhWSta/71W", "observer_02");
cc.Class({
extends: cc.Component,
properties: {
Subject_Node: cc.Node
},
onLoad: function() {
this.Subject_Node.on("RED", this.Become_Red, this);
},
Become_Red: function() {
this.node.color = cc.color(255, 0, 0, 255);
},
Become_Green: function() {
this.node.color = cc.color(0, 255, 0, 255);
}
});
cc._RF.pop();
}, {} ],
physics_enabler: [ function(t, e) {
"use strict";
cc._RF.push(e, "5235fXciNlJVoexMLolFo7q", "physics_enabler");
cc.Class({
extends: cc.Component,
properties: {
Debug_Draw: !1
},
onLoad: function() {
cc.director.getPhysicsManager().enabled = !0;
if (this.Debug_Draw) {
var t = cc.PhysicsManager.DrawBits;
cc.director.getPhysicsManager().debugDrawFlags = t.e_aabbBit | t.e_pairBit | t.e_centerOfMassBit | t.e_jointBit | t.e_shapeBit;
}
}
});
cc._RF.pop();
}, {} ],
player_control: [ function(t, e) {
"use strict";
cc._RF.push(e, "00dcfNVG+9KFJEqh8qB4tVW", "player_control");
cc.Class({
extends: cc.Component,
properties: {
Bomb_Prefab: cc.Prefab
},
onLoad: function() {
window.test_comp = this;
this.node.player_control = this;
this.Rigid_Body = this.node.getComponent(cc.RigidBody);
cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyPressed, this);
cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyReleased, this);
this.Direction = 0;
this.On_The_Ground = !1;
this.Velocity_Max_X = 400;
this.Walk_Force = 15e3;
this.Jump_Force = 5e5;
},
onKeyPressed: function(t) {
switch (t.keyCode) {
case cc.macro.KEY.left:
this.Direction = -1;
break;

case cc.macro.KEY.right:
this.Direction = 1;
break;

case cc.macro.KEY.up:
if (this.On_The_Ground) {
this.Rigid_Body.applyForceToCenter(cc.v2(0, this.Jump_Force), !0);
this.On_The_Ground = !1;
}
break;

case cc.macro.KEY.space:
this.Throw_Bomb();
}
},
onKeyReleased: function(t) {
switch (t.keyCode) {
case cc.macro.KEY.left:
case cc.macro.KEY.right:
this.Direction = 0;
}
},
onBeginContact: function(t, e) {
2 === e.tag && (this.On_The_Ground = !0);
},
update: function() {
(this.Direction > 0 && this.Rigid_Body.linearVelocity.x < this.Velocity_Max_X || this.Direction < 0 && this.Rigid_Body.linearVelocity.x > -this.Velocity_Max_X) && this.Rigid_Body.applyForceToCenter(cc.v2(this.Direction * this.Walk_Force, 0), !0);
},
Throw_Bomb: function() {
var t = cc.instantiate(this.Bomb_Prefab);
t.parent = this.node.parent;
var e = this.node.getPosition();
e.x += 70;
t.setPosition(e);
t.getComponent(cc.RigidBody).applyForceToCenter(cc.v2(14e4, 2e5), !0);
}
});
cc._RF.pop();
}, {} ],
player_demo: [ function(t, e) {
"use strict";
cc._RF.push(e, "addcdKptvlHPr/5EHdoEcGE", "player_demo");
var o = cc.Enum({
RED: -1,
GREEN: -1,
BLUE: -1
}), n = cc.Enum({
FIRE: -1,
SNOW: -1,
WATER: -1,
ICE: -1
}), i = cc.Class({
name: "PLAYER_INFO",
properties: {
Name: "Player Name",
Skin: {
default: o.RED,
type: o
},
Special_Attack: {
default: n.SNOW,
type: n
},
Max_Speed: cc.v2(100, 100)
}
});
cc.Class({
extends: cc.Component,
properties: {
Player_Info: {
default: null,
type: i
}
},
onLoad: function() {
cc.log("Special_Attack: " + Object.keys(n)[this.Player_Info.Special_Attack]);
}
});
cc._RF.pop();
}, {} ],
player: [ function(t, e) {
"use strict";
cc._RF.push(e, "042c1akaeJIybrPfPU2FiKb", "player");
cc.Class({
extends: cc.Component,
properties: {
Touch_Input: t("touch_input")
},
update: function(t) {
var e = this.node.getPosition();
e.addSelf(this.Touch_Input.Joystick_Vector.mul(3 * t));
this.node.setPosition(e);
}
});
cc._RF.pop();
}, {
touch_input: "touch_input"
} ],
popup_window: [ function(t, e) {
"use strict";
cc._RF.push(e, "4ea64oQgAZI/aT0xtC+fEXB", "popup_window");
cc.Class({
extends: cc.Component,
properties: {
Yes_Event_Handler: {
default: null,
type: cc.Component.EventHandler
},
No_Event_Handler: {
default: null,
type: cc.Component.EventHandler
}
},
Show_Window: function() {
this.node.active = !0;
this.node.opacity = 0;
this.node.scale = .2;
cc.tween(this.node).to(.5, {
scale: 1,
opacity: 255
}, {
easing: "quartInOut"
}).start();
},
Hide_Window: function() {
var t = this;
cc.tween(this.node).to(.5, {
scale: .2,
opacity: 0
}, {
easing: "quartInOut"
}).call(function() {
t.node.active = !1;
}).start();
},
Yes_Clicked: function() {
this.Yes_Event_Handler && this.Yes_Event_Handler.emit();
this.Hide_Window();
},
No_Clicked: function() {
this.No_Event_Handler && this.No_Event_Handler.emit();
this.Hide_Window();
}
});
cc._RF.pop();
}, {} ],
rope_part: [ function(t, e) {
"use strict";
cc._RF.push(e, "1ab49/YYT9HDbb+AaAPzaPI", "rope_part");
cc.Class({
extends: cc.Component,
properties: {
Anchor_Position: {
default: 1,
range: [ 0, 1 ],
notify: function() {
this.Update_Anchor();
}
},
Pos_Short: 0,
Pos_Long: 24
},
onLoad: function() {
this.Joints = this.node.getComponentsInChildren(cc.RevoluteJoint);
this.Joints.reverse();
},
Update_Anchor: function() {
for (var t = 1; t < this.Joints.length; t++) {
var e = this.Joints.length, o = cc.misc.clamp01((this.Anchor_Position - t * (1 / e)) * e), n = cc.misc.lerp(this.Pos_Short, this.Pos_Long, o);
this.Joints[t].anchor.y = n;
this.Joints[t].connectedAnchor.y = -n;
this.Joints[t].apply();
this.Joints[t].node.height = cc.misc.lerp(2 * n, 2 * this.Pos_Long + 12, .8);
}
}
});
cc._RF.pop();
}, {} ],
scene_transition: [ function(t, e) {
"use strict";
cc._RF.push(e, "2c358GcYnxNwpcuDdUySlyG", "scene_transition");
cc.Class({
extends: cc.Component,
properties: {},
onLoad: function() {
cc.game.addPersistRootNode(this.node);
},
Load_Next_Scene: function() {
var t = this;
cc.tween(this.node).to(1, {
position: cc.v2(640, 360)
}, {
easing: "cubicInOut"
}).call(function() {
t.Load_Scene();
}).to(1, {
position: cc.v2(-640, 360)
}, {
easing: "cubicInOut"
}).start();
},
Load_Scene: function() {
cc.director.loadScene("Scene_Trans_02");
}
});
cc._RF.pop();
}, {} ],
shader_control: [ function(t, e) {
"use strict";
cc._RF.push(e, "395c63ssElAEY0feYRJ8SnY", "shader_control");
cc.Class({
extends: cc.Component,
editor: {
executeInEditMode: !0
},
properties: {
Wipe: {
default: 0,
range: [ 0, 360 ],
notify: function() {
this.Update_Values();
}
}
},
onLoad: function() {
this.Sprite = this.node.getComponent(cc.Sprite);
this.Sprite_Frame = this.Sprite.spriteFrame;
this.Material = this.Sprite.getMaterial(0);
window.sprt = this.Sprite;
this.Update_Values();
},
Update_Values: function() {
this.Material.setProperty("rx", this.Sprite_Frame._rect.x, 0);
this.Material.setProperty("ry", this.Sprite_Frame._rect.y, 0);
this.Material.setProperty("rw", this.Sprite_Frame._rect.width, 0);
this.Material.setProperty("rh", this.Sprite_Frame._rect.height, 0);
this.Material.setProperty("tw", this.Sprite_Frame._texture.width, 0);
this.Material.setProperty("th", this.Sprite_Frame._texture.height, 0);
this.Material.setProperty("wipe", this.Wipe, 0);
}
});
cc._RF.pop();
}, {} ],
status_control: [ function(t, e) {
"use strict";
cc._RF.push(e, "cb8d4tOEJpJmIppQ38EJqeE", "status_control");
cc.Class({
extends: cc.Component,
properties: {
Color_Box: {
default: null,
type: cc.Node
}
},
YESSSSS: function() {
this.Color_Box.color = cc.color(0, 255, 0);
},
NOOOOOOO: function() {
this.Color_Box.color = cc.color(255, 0, 0);
}
});
cc._RF.pop();
}, {} ],
touch_input: [ function(t, e) {
"use strict";
cc._RF.push(e, "cf081hZBWhPsK198c8GkTkh", "touch_input");
cc.Class({
extends: cc.Component,
properties: {
Joystick_Ball: cc.Node,
Joystick_Vector: cc.v2(),
Joystick_Max: 100
},
onLoad: function() {
this.node.on(cc.Node.EventType.TOUCH_START, this.Joystick_Touch_Start, this);
this.node.on(cc.Node.EventType.TOUCH_MOVE, this.Joystick_Touch_Move, this);
this.node.on(cc.Node.EventType.TOUCH_END, this.Joystick_Touch_End, this);
this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.Joystick_Touch_End, this);
},
Joystick_Touch_Start: function(t) {
var e = t.getLocation(), o = this.node.convertToNodeSpaceAR(e);
this.Limit_joystick_Vector(o);
this.Set_Joystick_Ball_Position(o);
this.Joystick_Vector = o;
},
Joystick_Touch_Move: function(t) {
var e = t.getTouches()[0].getLocation(), o = this.node.convertToNodeSpaceAR(e);
this.Limit_joystick_Vector(o);
this.Set_Joystick_Ball_Position(o);
this.Joystick_Vector = o;
},
Joystick_Touch_End: function() {
this.Joystick_Vector = cc.Vec2.ZERO;
this.Set_Joystick_Ball_Position(cc.Vec2.ZERO);
},
Set_Joystick_Ball_Position: function(t) {
this.Joystick_Ball.setPosition(t);
},
Limit_joystick_Vector: function(t) {
var e = t.mag();
e > this.Joystick_Max && t.mulSelf(this.Joystick_Max / e);
}
});
cc._RF.pop();
}, {} ],
touch_listener: [ function(t, e) {
"use strict";
cc._RF.push(e, "a23afnfy+hA26bDuppuXF7O", "touch_listener");
cc.Class({
extends: cc.Component,
properties: {
My_Button: cc.Button
},
onLoad: function() {
this.node.on(cc.Node.EventType.TOUCH_START, this.Touch_Start, this);
this.My_Button._onTouchBegan = function() {
if (this.interactable && this.enabledInHierarchy) {
this._pressed = !0;
this._updateState();
}
};
},
Touch_Start: function(t) {
var e = t.getLocation();
cc.log("X: " + e.x + " - Y: " + e.y);
}
});
cc._RF.pop();
}, {} ],
tween_test: [ function(t, e) {
"use strict";
cc._RF.push(e, "0d167WUMb1CeawebPW1Nf2u", "tween_test");
cc.Class({
extends: cc.Component,
properties: {},
start: function() {
cc.tween(this.node).repeatForever(cc.tween().to(1, {
scale: 2,
color: cc.color(255, 0, 0, 255)
}, {
easing: "quadInOut"
}).call(function() {
cc.log("EXPLOSION!");
}).to(1, {
scale: .7,
color: cc.color(255, 255, 255, 255)
}, {
easing: "quadInOut"
})).start();
}
});
cc._RF.pop();
}, {} ]
}, {}, [ "rope_part", "Animation_Control", "touch_listener", "DataManager", "EventSystem", "GameConstants", "GameManager", "Grid", "NodeNumber", "SoundManager", "UIManager", "Test", "player_demo", "event_demo", "observer_01", "observer_02", "glass_control", "bomb_control", "camera_control", "explosion_force", "main_game_control", "player_control", "debug_body", "physics_enabler", "scene_transition", "bomb_control_2", "explosion_effect", "shader_control", "Button_Test", "Swipe_Camera", "tween_test", "popup_window", "status_control", "my_progress_bar", "player", "touch_input" ]);