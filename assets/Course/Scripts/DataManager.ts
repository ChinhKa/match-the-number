// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameManager from "./GameManager";
import NodeNumber from "./NodeNumber";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DataManager extends cc.Component {

    public static ins: DataManager = null;

    onLoad() {
        DataManager.ins = this;
        this.LoadData();
    }

    listNodeSaved: Object[] = [];

    listReward: Object[] = [{
        'size': 512,
        'reward': 100
    }, {
        'size': 1024,
        'reward': 200
    }, {
        'size': 2048,
        'reward': 300
    }, {
        'size': 4096,
        'reward': 400
        }]

    update(dt) {
        if (GameManager.ins.endGame) {
            cc.sys.localStorage.removeItem("objectData");
        }
    }

    SaveData() {
        if (!GameManager.ins.endGame) {
            setTimeout(() => {
                this.listNodeSaved.splice(0, DataManager.ins.listNodeSaved.length);
                for (let i = 0; i < GameManager.ins.listNode.length; i++) {

                    this.listNodeSaved.push({ number: GameManager.ins.listNode[i].getComponent(NodeNumber).number, position: GameManager.ins.listNode[i].position });
                }
                cc.sys.localStorage.setItem("objectData", JSON.stringify(this.listNodeSaved));
            }, 500);
        }
    }

    LoadData() {
        const savedData = cc.sys.localStorage.getItem("objectData");
        if (savedData !== null) {
            this.listNodeSaved = JSON.parse(savedData);
        }

        setTimeout(() => {
            GameManager.ins.SpawnNode(5, 2);
        }, 500);
    }

    ResetData() {
        cc.sys.localStorage.removeItem("objectData");   
        GameManager.ins.ReloadScene();
    }
}
