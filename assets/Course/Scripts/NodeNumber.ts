﻿// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { Console } from "console";
import GameManager from "./GameManager";
import Grid from "./Grid";
import SoundManager from "./SoundManager";
import UIManager from "./UIManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NodeNumber extends cc.Component {
    @property(cc.Label)
    txtNumber: cc.Label = null;

    @property
    number: number = null;
    ID: number = null;
    isDragging: boolean = false;
    isDropped: boolean = false;
    canStillPull: boolean = false;
    touchOffset: cc.Vec3 = cc.Vec3.ZERO;
    old_PosX: number = null;
    enabledDrop: boolean = true;
    posReferance: cc.Vec3;
    minX:number = -340; 
    maxX:number = 340;
    minY:number = -300;
    maxY: number = 720;
    existed: boolean = true;
    @property(cc.BoxCollider)
    col_Sensor: cc.BoxCollider;
    nodeCollision: cc.Vec3 = cc.Vec3.ZERO;
    onLoad() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }
   
    onTouchStart(event) {
        if (!GameManager.ins.endGame) {
            SoundManager.ins.PlayPopClip();
            this.posReferance = this.node.position;
            GameManager.ins.enablePush = true;
            this.col_Sensor.enabled = false;
            this.old_PosX = this.node.position.x;
            if (GameManager.ins.useBreakItem) {
                GameManager.ins.useBreakItem = false;
                this.DestroyNode();
                GameManager.ins.DropDownNode(-1);
            } else if (GameManager.ins.useSwapItem) {
                if (GameManager.ins.listNode_Swap.length <= 2) {
                    cc.tween(this.node)
                        .to(0, { scale: 1.1 })
                        .start()
                    GameManager.ins.listNode_Swap.push(this.node);
                    GameManager.ins.SwapItems();
                }
                return;
            }
            this.Hover();
            for (let i = 0; i < GameManager.ins.queue.length; i++) {
                if (GameManager.ins.queue[i].isPlaced && Math.round(GameManager.ins.queue[i].position.x) === Math.round(this.node.position.x)
                    && Math.round(GameManager.ins.queue[i].position.y) === Math.round(this.node.position.y)) {
                    GameManager.ins.queue[i].isPlaced = false;
                    break;
                }
            }
            this.isDragging = true;
            this.isDropped = false;
            const touchPos = event.getLocation();
            this.touchOffset = this.node.position.sub(this.node.parent.convertToNodeSpaceAR(touchPos));
        }
    }

    onTouchMove(event) {
        if (this.isDragging && !GameManager.ins.endGame) {
            const touchPos = event.getLocation();
            const newPosition = this.node.parent.convertToNodeSpaceAR(touchPos).add(this.touchOffset);

            var nodeUp = GameManager.ins.queue.find(n => n.position.x === this.posReferance.x && n.position.y === this.posReferance.y + 170);
            var nodeDown = GameManager.ins.queue.find(n => n.position.x === this.posReferance.x && n.position.y === this.posReferance.y - 170);
            var nodeLeft = GameManager.ins.queue.find(n => n.position.x === this.posReferance.x - 170 && n.position.y === this.posReferance.y);
            var nodeRight = GameManager.ins.queue.find(n => n.position.x === this.posReferance.x + 170 && n.position.y === this.posReferance.y);

            if (nodeUp === undefined || nodeUp.isPlaced) {              
                this.maxY = this.posReferance.y;              
            } else {    
                this.maxY = 720;
            }

            if (nodeDown === undefined || nodeDown.isPlaced) {                
                if (nodeDown !== undefined) {
                    var rs = GameManager.ins.listNode.find(n => n.position.x === nodeDown.position.x && n.position.y === nodeDown.position.y && n.getComponent(NodeNumber).number === this.number);
                    if (rs === undefined) {
                        this.minY = this.posReferance.y;
                    } else {
                        this.minY = -300;
                    }
                } else {
                        this.minY = this.posReferance.y;
                }
            } else {
                this.minY = -300;
            }

            if (nodeLeft === undefined || nodeLeft.isPlaced) {
                if (nodeLeft !== undefined) {
                    var rs = GameManager.ins.listNode.find(n => n.position.x === nodeLeft.position.x && n.position.y === nodeLeft.position.y && n.getComponent(NodeNumber).number === this.number);
                    if (rs === undefined) {
                        this.minX = this.posReferance.x;                                   
                    } else {
                        this.minX = -340;
                    }
                } else {
                    this.minX = this.posReferance.x;                                   
                }
            } else {
                this.minX = -340;
            }

            if (nodeRight === undefined || nodeRight.isPlaced) {
                if (nodeRight !== undefined) {
                    var rs = GameManager.ins.listNode.find(n => n.position.x === nodeRight.position.x && n.position.y === nodeRight.position.y && n.getComponent(NodeNumber).number === this.number);
                    if (rs === undefined) {
                        this.maxX = this.posReferance.x;
                    } else {
                        this.maxX = 340;
                    }
                } else {
                    this.maxX = this.posReferance.x;
                }
            } else {
                this.maxX = 340;
            }


            if (newPosition.x < this.minX) newPosition.x = this.minX;
            if (newPosition.x > this.maxX) newPosition.x = this.maxX;
            if (newPosition.y < this.minY) newPosition.y = this.minY;
            if (newPosition.y > this.maxY) newPosition.y = this.maxY;

 
            this.node.position = newPosition;
            if (this.posReferance.x !== this.old_PosX) {
                GameManager.ins.DropDownNode_C(this.ID,this.old_PosX);
            }
        }
    }
    onTouchEnd() {
        if (!GameManager.ins.useSwapItem) {
            cc.tween(this.node)
                .to(0, { scale: 1 })
                .start()
        }
        this.isDragging = false;
        this.isDropped = true;
        this.enabledDrop = true;
        this.col_Sensor.enabled = true;

        setTimeout(() => {
            if (Math.round(this.node.position.x) !== this.old_PosX && GameManager.ins.enablePush) {
                GameManager.ins.PushNode();
            }
        },200);         
 
        GameManager.ins.DropDownNode(this.ID);
    }
     
    onTouchCancel() {
        this.isDropped = true;
        this.isDragging = false;
        this.enabledDrop = true;
        GameManager.ins.DropDownNode(-1);
    }

    onDestroy() {
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    setNumber(nbr) {
        this.number = nbr;
        this.txtNumber.string = nbr + "";
    }

    onCollisionStay(other, self) {
        if (!GameManager.ins.endGame) {
            if (this.isDropped) {
                this.isDropped = false;

                if (other.tag === 1) {
                    if (!other.node.getComponent(Grid).isPlaced) {
                        this.node.setPosition(other.node.getPosition());
                    } else {
                        this.node.setPosition(this.posReferance);
                    }
                    this.DropDown();
                }
            }

            if (other.tag === 0) {
                if (this.isDragging && !other.node.getComponent(NodeNumber).isDragging) {
                    if (this.number === other.node.getComponent(NodeNumber).number) {
                        SoundManager.ins.PlayEatingClip();
                        let nbrs = this.number * 2;

                        GameManager.ins.SpawnComboEff(this.node.position);
                        other.node.getComponent(NodeNumber).setNumber(nbrs);
                        other.node.getComponent(NodeNumber).Hover();
                        let getPoint = Number(cc.sys.localStorage.getItem('currentPoint'));
                        let newPoint = getPoint + nbrs;
                        cc.sys.localStorage.setItem('currentPoint', newPoint);

                        let getMaxPoint = Number(cc.sys.localStorage.getItem('maxPoint'));
                        if (getMaxPoint <= newPoint) {
                            cc.sys.localStorage.setItem('maxPoint', newPoint);
                        }

                        this.DestroyNode();
                        GameManager.ins.CheckReward(nbrs);
                        UIManager.ins.ShowText();
                        GameManager.ins.SelectNodeColor(other.node);
                        GameManager.ins.DropDownNode(-1);
                        GameManager.ins.enablePush = false;
                    }  
                }
            }else if (other.tag === 6) {
                if (!this.isDragging && !other.node.getComponent(NodeNumber).isDragging) {
                    if (this.number === other.node.getComponent(NodeNumber).number) {
                        SoundManager.ins.PlayEatingClip();
                        let nbrs = this.number * 2;
                        GameManager.ins.SpawnComboEff(this.node.position);
                        other.node.getComponent(NodeNumber).setNumber(nbrs);
                        other.node.getComponent(NodeNumber).Hover();
                        let getPoint = Number(cc.sys.localStorage.getItem('currentPoint'));
                        let newPoint = getPoint + nbrs;
                        cc.sys.localStorage.setItem('currentPoint', newPoint);

                        let getMaxPoint = Number(cc.sys.localStorage.getItem('maxPoint'));
                        if (getMaxPoint <= newPoint) {
                            cc.sys.localStorage.setItem('maxPoint', newPoint);
                        }

                        this.DestroyNode();
                        GameManager.ins.CheckReward(nbrs);
                        UIManager.ins.ShowText();
                        GameManager.ins.SelectNodeColor(other.node);
                        GameManager.ins.DropDownNode(-1);
                        GameManager.ins.enablePush = false;
                    }
                }
            }
        }        
            if (other.tag === 1) {
                if (!other.node.getComponent(Grid).isPlaced) {
                    this.posReferance = other.node.position;
                }
            }
    }

    Hover() {
        cc.tween(this.node)
            .to(0.1, { scale: 1.2 }).call(() => {
                cc.tween(this.node)
                    .to(0.1, { scale: 1 }).start()
            })
            .start()
    }

    DropDown() {
        if (!this.isDragging) {
            for (let i = GameManager.ins.queue.length - 1; i >= 0; i--) {
                GameManager.ins.CheckGridPlaced();
                if (!GameManager.ins.queue[i].isPlaced && Math.round(GameManager.ins.queue[i].position.x) === Math.round(this.node.position.x)) {
                    this.node.setPosition(new cc.Vec2(this.node.position.x, GameManager.ins.queue[i].position.y));
                }
            }
        }
    }

    DestroyNode() {
        const resultFind = GameManager.ins.listNode.findIndex(n => n.position.x === this.node.position.x && n.position.y === this.node.position.y);
        GameManager.ins.listNode.splice(resultFind, 1);
        this.node.destroy();
    }
}
