﻿// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { match } from "node:assert/strict";
import { Console, time } from "node:console";
import { parse } from "path/posix";
import DataManager from "./DataManager";
import Grid from "./Grid";
import NodeNumber from "./NodeNumber";
import Popup from "./Popup";
import SoundManager from "./SoundManager";
import UIManager from "./UIManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameManager extends cc.Component {
    public static ins: GameManager = null;

    @property(cc.Label)
    label: cc.Label = null;

    redColor: cc.Color = cc.Color.RED
    blueColor: cc.Color = cc.Color.BLUE
    greenColor: cc.Color = cc.Color.GREEN
    yellowColor: cc.Color = cc.Color.YELLOW
    orangeColor: cc.Color = cc.Color.ORANGE;
    cyanColor: cc.Color = cc.Color.CYAN;
    grayColor: cc.Color = cc.Color.GRAY;

    @property
    listNumber: Number[] = [2, 4, 8, 16, 32, 64, 128]

    @property(cc.Prefab)
    pre_Node: cc.Prefab = null;

    @property(cc.Prefab)
    pre_Grid: cc.Prefab = null;

    queue: Grid[] = [];
    listNode: cc.Node[] = [];

    endGame: boolean = false;
    useBreakItem: boolean = false;
    useSwapItem: boolean = false;

    listNode_Swap: cc.Node[] = [];
    node_Temp: cc.Vec3;    
    isPushing: boolean;
    isFull: boolean;
    @property(cc.Prefab)
    comboPre: cc.Prefab = null;
    numbersCombo: number = 0;
    countDownCombo: number = 0;
    enablePush: boolean = true;
    gameStarted: boolean = false;
    onLoad() {
        GameManager.ins = this;
    }

    start() {
        this.SpawnGrid(5, 7);
        this.CheckGridPlaced();
    }

    update(dt) {
        if (this.countDownCombo <= 0) {
            this.numbersCombo = 0;
        } else {
            this.countDownCombo -= dt;
        }
        if (this.isFull) {
             
        }
    }

    SpawnGrid(w, h) {
        for (var i = 0; i < w; i++) {
            for (var j = 0; j < h; j++) {
                let node = cc.instantiate(this.pre_Grid);
                this.node.addChild(node);
                node.setPosition(cc.v2(i * 170 - 340, j * 170 - 300));
                node.getComponent(Grid).isPlaced = false;
                node.getComponent(Grid).position = node.position;
                this.queue.push(node.getComponent(Grid));
            }
        }
    }

    SpawnComboEff(pos) {
        this.countDownCombo = 1;
        this.numbersCombo++;
        let txtCombo;
        let node = cc.instantiate(this.comboPre);
        this.node.addChild(node);
        node.setPosition(pos);
        if (this.numbersCombo <= 2) {
            txtCombo = "Combo X" + this.numbersCombo;
            node.color = this.greenColor;
        } else if (this.numbersCombo <= 3) {
            txtCombo = "Good!";
            node.color = this.yellowColor;
        } else if (this.numbersCombo <= 4) {
            txtCombo = "Great!";
            node.color = this.orangeColor;
        } else if (this.numbersCombo <= 5) {
            txtCombo = "Perfect!";
            node.color = this.redColor;
        }
        node.getComponent(Popup).txtCombo.string = txtCombo;
    }

    SpawnNode(w, h) {
        let idx = 0;
        if (cc.sys.localStorage.getItem("objectData") === null) {
            for (var i = 0; i < w; i++) {
                for (var j = 0; j < h; j++) {
                    let node = cc.instantiate(this.pre_Node);
                    var idxR = Math.floor(Math.random() * (this.listNumber.length - 0)) + 0;
                    node.getComponent(NodeNumber).setNumber(this.listNumber[idxR]);
                    node.getComponent(NodeNumber).ID = idx;
  
                    this.listNode.push(node);
                    this.SelectNodeColor(node);
                    this.node.addChild(node);
                    node.setPosition(cc.v2(i * 170 - 340, j * 170 - 300));
                    node.scale = 0;
                    idx++;
                }
            }
        } else {
            for (let j = 0; j < DataManager.ins.listNodeSaved.length; j++) {
                let node = cc.instantiate(this.pre_Node);
                node.getComponent(NodeNumber).setNumber(DataManager.ins.listNodeSaved[j]['number']);
                node.getComponent(NodeNumber).ID = idx;
                this.listNode.push(node);
                this.SelectNodeColor(node);
                this.node.addChild(node);
                node.setPosition(DataManager.ins.listNodeSaved[j]['position']);
                node.scale = 0;
                idx++;
            }
        }
        for (let n = 0; n < this.listNode.length; n++) {
            setTimeout(() => {
                cc.tween(this.listNode[n])
                    .to(0.3, { scale: 1 }, { easing: "quadInOut" })
                    .start()
            }, n * 10);
        }

        this.CheckGridPlaced();
    }

    Check() {
        let i = 0;
        for (let q = 0; q < this.queue.length; q++) {
            console.log(q + "+" + this.queue[q].isPlaced);
            i++;
        }
    }

    CheckGridPlaced() {
        try {
            for (let q of this.queue) {
                q.isPlaced = false;
            }
            let count = 0;
            for (let q of this.queue) {
                for (let n of this.listNode) {
                    if (Math.round(q.position.x) === Math.round(n.position.x)
                        && Math.round(q.position.y) === Math.round(n.position.y)) {
                        q.isPlaced = true;
                        count++;
                        break;
                    }
                }
            }            

            if (count >= this.queue.length) {
                UIManager.ins.ActiveUI(UIManager.ins.endGameUI);
                SoundManager.ins.PlayEndGame();
                this.endGame = true;
            }
 
            DataManager.ins.SaveData();            
        } catch (e) {
            console.log("Lỗi" + e);
        }
    }

    SelectNodeColor(node) {
        switch (node.getComponent(NodeNumber).number) {
            case 2:
                node.color = this.redColor;
                break;
            case 4:
                node.color = this.blueColor;
                break;
            case 8:
                node.color = this.greenColor;
                break;
            case 16:
                node.color = this.yellowColor;
                break;
            case 32:
                node.color = this.orangeColor;
                break;
            case 64:
                node.color = this.cyanColor;
                break;
            case 128:
                node.color = this.grayColor;
                break;
            default:
                node.color = this.grayColor;
                break;
        }
    }

    DropDownNode(id) {
        for (let i = 0; i < this.listNode.length; i++) {
            if (this.listNode[i].getComponent(NodeNumber).ID != id) {
                this.listNode[i].getComponent(NodeNumber).DropDown();
            }
        }
    }

    DropDownNode_C(id,posX) {
        for (let i = 0; i < this.listNode.length; i++) {
            if (this.listNode[i].getComponent(NodeNumber).ID != id && this.listNode[i].position.x === posX) {
                this.listNode[i].getComponent(NodeNumber).DropDown();
            }
        }
    }

    PushNode() {
        setTimeout(() => {
            if (!this.endGame && this.enablePush) {
                for (let j = 6; j < 41; j += 7) {
                    this.CheckGridPlaced();
                    if (!this.queue[j].isPlaced) {
                        for (let i = 0; i < this.listNode.length; i++) {
                            if (this.queue[j].position.x === this.listNode[i].position.x) {
                                this.listNode[i].setPosition(new cc.Vec3(this.listNode[i].position.x, this.listNode[i].position.y + 170, 0));
                            }
                        }

                        let node = cc.instantiate(this.pre_Node);
                        var idxR = Math.floor(Math.random() * (this.listNumber.length - 0)) + 0;
                        node.getComponent(NodeNumber).setNumber(this.listNumber[idxR]);
                        this.listNode.push(node);
                        this.SelectNodeColor(node);
                        this.node.addChild(node);
                        node.setPosition(this.queue[j - 6].position);
                    }
                }
            }  
        }, 200);
    }

    UseBreakItem() {
        SoundManager.ins.PlayExplodeClip();
        this.useBreakItem = true;
        this.UpdateCoin(115);

    }

    UseSwapItem() {
        SoundManager.ins.PlaySwapClip();
        this.useSwapItem = true;
        this.UpdateCoin(115);

    }

    AddCoin() {
        this.UpdateCoin(-111);
    }

    UpdateCoin(value) {
        let coin = parseInt(cc.sys.localStorage.getItem('coin'));

        if (value <= coin) {
            cc.sys.localStorage.setItem('coin', (coin - value));
            UIManager.ins.ShowText();
        }   
    }

    SwapItems() {
        if (this.listNode_Swap.length >= 2 && this.useSwapItem) {            
            this.useSwapItem = false;
            this.node_Temp = this.listNode_Swap[0].position;
            this.listNode_Swap[0].position = this.listNode_Swap[1].position;
            this.listNode_Swap[0].getComponent(NodeNumber).posReferance = this.listNode_Swap[1].position;
            this.listNode_Swap[1].position = this.node_Temp;
            this.listNode_Swap[1].getComponent(NodeNumber).posReferance = this.node_Temp;
 
            cc.tween(this.listNode_Swap[0])
                .to(0, { scale: 1 })
                .start()

            cc.tween(this.listNode_Swap[1])
                .to(0, { scale: 1 })
                .start()
 

            while (this.listNode_Swap.length > 0) {               
                this.listNode_Swap.pop();
            }
        }
    }

    CheckReward(nbr) {        
        for (let i = 0; i < DataManager.ins.listReward.length; i++) {
            if (parseInt(DataManager.ins.listReward[i]['size']) === parseInt(nbr)) {
                let coin = parseInt(cc.sys.localStorage.getItem('coin'));
                let newCoin = coin + parseInt(DataManager.ins.listReward[i]['reward']);
                cc.sys.localStorage.setItem('coin', newCoin);
            }
        }   
    }

    ReloadScene() {
        cc.sys.localStorage.setItem('currentPoint', 0);
        cc.director.loadScene("SceneMain");
    }

    CheckLose() {
        for (let i = 0; i < this.listNode.length; i++) {
            
        }
    }
}