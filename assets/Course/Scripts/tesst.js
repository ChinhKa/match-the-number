﻿// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    update(dt) {
        
            const physicsManager = cc.director.getPhysicsManager();
            const hitResults = physicsManager.rayCast(new cc.Vec2(this.node.position.x, this.node.position.y), new cc.Vec2(0, 1), cc.RayCastType.All);

 
            for (const result of hitResults) {
                if (result.collider.node !== this.node) {
                    console.log("Okela");
                  
                }
            }
            console.log("Okela2");
        
    }
});
