// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class SoundManager extends cc.Component {
    public static ins: SoundManager = null;
    @property(cc.AudioSource)
    audioSorce: cc.AudioSource;
    @property(cc.AudioClip)
    eatingClip: cc.AudioClip;
    @property(cc.AudioClip)
    gameOverClip: cc.AudioClip;
    @property(cc.AudioClip)
    explodeClip: cc.AudioClip;
    @property(cc.AudioClip)
    swapClip: cc.AudioClip;
    @property(cc.AudioClip)
    popClip: cc.AudioClip;

    onLoad() {
        SoundManager.ins = this;
    }

    start () {

    }

    Mute() {

    }

    UnMute(){

    }

    Vibrate() {

    }

    UnVibrate() {

    }

    PlayEatingClip() {
        cc.audioEngine.play(this.eatingClip,false,1);
    }

    PlayExplodeClip() {
        cc.audioEngine.play(this.explodeClip, false, 1);
    }

    PlaySwapClip() {
        cc.audioEngine.play(this.swapClip, false, 1);
    }

    PlayPopClip() {
        cc.audioEngine.play(this.popClip, false, 1);
    }

    PlayEndGame() {
        cc.audioEngine.play(this.gameOverClip, false, 1);        
    }
}
