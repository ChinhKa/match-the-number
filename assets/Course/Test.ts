﻿// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    @property
    speed: number = 200;

    onLoad() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        cc.director.getPhysicsManager().enabled = true;
    }

    start() {

    }

    onKeyDown(event: cc.Event.EventKeyboard) {
        switch (event.keyCode) {
            case cc.macro.KEY.left:
                this.node.x -= this.speed * cc.director.getDeltaTime();
                break;
            case cc.macro.KEY.right:
                this.node.x += this.speed * cc.director.getDeltaTime();
                break;
            case cc.macro.KEY.up:
                this.node.y += this.speed * cc.director.getDeltaTime();
                break;
            case cc.macro.KEY.down:
                this.node.y -= this.speed * cc.director.getDeltaTime();
                break;
        }
    }

    update(dt) {

    }

    onKeyUp(event: cc.Event.EventKeyboard) {
        // Xử lý sự kiện khi nút được nhả ra (nếu cần)
    }

    onDestroy() {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    onCollisionEnter(other, self) { 
        console.log(123);
    }
}
