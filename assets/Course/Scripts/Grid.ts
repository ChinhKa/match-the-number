// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameManager from "./GameManager";
import NodeNumber from "./NodeNumber";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Grid extends cc.Component {
    isPlaced: boolean = null;
    position: cc.Vec3 = null;
}
