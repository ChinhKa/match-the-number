// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
@ccclass
export default class UIManager extends cc.Component {
    public static ins: UIManager = null;

    @property(cc.Node)
    endGameUI: cc.Node = null;

    @property(cc.Node)
    winGameUI: cc.Node = null;

    @property(cc.Node)
    pauseGameUI: cc.Node = null;

    @property(cc.Node)
    homeGameUI: cc.Node = null;

    @property(cc.Label)
    txtCoin: cc.Label = null;

    @property(cc.Label)
    txtMaxPoint: cc.Label = null;

    @property(cc.Label)
    txtCurrentPoint: cc.Label = null;

    @property(cc.Label)
    txtCombo: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        UIManager.ins = this;
        this.ActiveUI(this.homeGameUI);
        this.pauseGameUI.active = false;
    }

    start() {
        if (cc.sys.localStorage.getItem('coin') === null) {
            cc.sys.localStorage.setItem('coin', 500);
        }
        if (cc.sys.localStorage.getItem('maxPoint') === null) {
            cc.sys.localStorage.setItem('maxPoint', 0);
        }
        if (cc.sys.localStorage.getItem('currentPoint') === null) {
            cc.sys.localStorage.setItem('currentPoint', 0);
        }
        if (cc.sys.localStorage.getItem('endTurn') === null) {
            cc.sys.localStorage.setItem('endTurn', 0);
        }
        if (cc.sys.localStorage.getItem('goal') === null) {
            cc.sys.localStorage.setItem('goal', -1);
        }
        this.ShowText();
    }

    ShowText() {
        this.txtCoin.string = cc.sys.localStorage.getItem('coin');
        this.txtMaxPoint.string = cc.sys.localStorage.getItem('maxPoint');
        this.txtCurrentPoint.string = cc.sys.localStorage.getItem('currentPoint');
    }

    ActiveUI(ui) {
        this.endGameUI.active = false;
        this.winGameUI.active = false;
        this.homeGameUI.active = false;
        
        if (ui != null) {
            ui.active = true;
        }
    }

    ShowPauseUI() {
        this.pauseGameUI.active = true;
    }

    HiddenPauseUI() {
        this.pauseGameUI.active = false;
        this.ActiveUI(this.homeGameUI);
    }
}
